package com.example.plusgameproject

import android.app.Activity
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.TextView
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {
    private val REQUEST_MAIN = 11
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val btnPlus = findViewById<Button>(R.id.btnPlus)
        btnPlus.setOnClickListener{
            val intent = Intent(MainActivity@this, PlusGameActivity::class.java)
            intent.putExtra("correct",txtAmoutCorrectMain.text)
            intent.putExtra("wrong",txtAmoutWrongMain.text)
            startActivityForResult(intent,REQUEST_MAIN)

//            val correct: String = intent.getStringExtra("correct")?: ""
//            val txtAmoutCorrectMain = findViewById<TextView>(R.id.txtAmoutCorrectMain)
//            txtAmoutCorrectMain.text = correct
//            val wrong: String = intent.getStringExtra("wrong")?: ""
//            val txtAmoutWrongMain = findViewById<TextView>(R.id.txtAmoutWrongMain)
//            txtAmoutWrongMain.text = wrong
//            startActivity(intent)
        }

        val btnMinus = findViewById<Button>(R.id.btnMinus)
        btnMinus.setOnClickListener {
            val intent = Intent(MainActivity@this, MinusActivity::class.java)
            intent.putExtra("correct2", txtAmoutCorrectMain2.text);
            intent.putExtra("wrong2", txtAmoutWrongMain2.text);
            startActivityForResult(intent,REQUEST_MAIN)
//            startActivity(intent)
        }

        val btnMulti = findViewById<Button>(R.id.btnMulti)
        btnMulti.setOnClickListener {
            val intent = Intent(MainActivity@this, MultipliedActivity::class.java)
            intent.putExtra("correct3", txtAmoutCorrectMain3.text);
            intent.putExtra("wrong3", txtAmoutWrongMain3.text);
            startActivityForResult(intent,REQUEST_MAIN)
//            startActivity(intent)
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == REQUEST_MAIN) {
            if (resultCode == Activity.RESULT_OK) {
                txtAmoutCorrectMain.text =  data?.getStringExtra("correct")
                txtAmoutWrongMain.text =  data?.getStringExtra("wrong")
                txtAmoutCorrectMain2.text =  data?.getStringExtra("correct2")
                txtAmoutWrongMain2.text =  data?.getStringExtra("wrong2")
                txtAmoutCorrectMain3.text =  data?.getStringExtra("correct3")
                txtAmoutWrongMain3.text =  data?.getStringExtra("wrong3")
            } else if (resultCode == Activity.RESULT_CANCELED) {
                //TODO Handle Result Cancel
            }
        }

    }
}