package com.example.plusgameproject

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.TextView
import kotlinx.android.synthetic.main.activity_minus.*
import kotlin.random.Random

class MinusActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_minus)

        PlayMinus()
        btnNextMinus.setOnClickListener {
            PlayMinus()
            txtAns.setText(" ")
        }
        btnHome2.setOnClickListener {
//            val intent = Intent(MinusActivity@this, MainActivity::class.java)
//            startActivity(intent)
            val txtAmoutCorrect = findViewById<TextView>(R.id.txtAmoutCorrect)
            val txtAmoutWrong = findViewById<TextView>(R.id.txtAmoutWrong)
            val intent = Intent()
            intent.putExtra("correct2", txtAmoutCorrect.text);
            intent.putExtra("wrong2", txtAmoutWrong.text);
            setResult(RESULT_OK, intent);
            finish();
        }
    }

    private fun PlayMinus() {
        val random1 = Random.nextInt(10) + 1;
        txt1.setText(Integer.toString(random1));

        val random2 = Random.nextInt(10) + 1;
        txt2.setText(Integer.toString(random2));

        val sum = random1 - random2

        val posit = Random.nextInt(3) + 1;
        if (posit == 1) {
            btnMinus1.setText(Integer.toString(sum));
            btnMinus2.setText(Integer.toString(sum + 1));
            btnMinus3.setText(Integer.toString(sum + 2));
        } else if (posit == 2) {
            btnMinus1.setText(Integer.toString(sum - 1));
            btnMinus2.setText(Integer.toString(sum));
            btnMinus3.setText(Integer.toString(sum + 1));

        } else {
            btnMinus1.setText(Integer.toString(sum - 2));
            btnMinus2.setText(Integer.toString(sum - 1));
            btnMinus3.setText(Integer.toString(sum));
        }

        btnMinus1.setOnClickListener {
            if (btnMinus1.text.toString().toInt() == sum) {
                txtAns.setText("ถูกต้อง")
                txtAmoutCorrect.text =
                    (txtAmoutCorrect.text.toString().toInt() + 1).toString()
            } else {
                txtAns.setText("ผิด")
                txtAmoutWrong.text = (txtAmoutWrong.text.toString().toInt()).toString()
            }
        }
        btnMinus2.setOnClickListener {
            if (btnMinus2.text.toString().toInt() == sum) {
                txtAns.setText("ถูกต้อง")
                txtAmoutCorrect.text =
                    (txtAmoutCorrect.text.toString().toInt() + 1).toString()
            } else {
                txtAns.setText("ผิด")
                txtAmoutWrong.text =
                    (txtAmoutWrong.text.toString().toInt() + 1).toString()
            }
        }
        btnMinus3.setOnClickListener {
            if (btnMinus3.text.toString().toInt() == sum) {
                txtAns.setText("ถูกต้อง")
                txtAmoutCorrect.text =
                    (txtAmoutCorrect.text.toString().toInt() + 1).toString()
            } else {
                txtAns.setText("ผิด")
                txtAmoutWrong.text =
                    (txtAmoutWrong.text.toString().toInt() + 1).toString()
            }
        }
    }
}