package com.example.plusgameproject

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_multiplied.*
import kotlin.random.Random


class MultipliedActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_multiplied)

        PlayMulti()
        btnNextMulti.setOnClickListener {
            PlayMulti()
            txtAns3.setText("")
        }
        btnHome3.setOnClickListener {
            val intent = Intent()
            intent.putExtra("correct3",txtAmoutCorrect3.text)
            intent.putExtra("wrong3",txtAmoutWrong3.text)
            setResult(RESULT_OK, intent);
            finish();
        }


    }

    private fun PlayMulti() {
        val random1 = Random.nextInt(10) + 1;
        txtMul1.setText(Integer.toString(random1));

        val random2 = Random.nextInt(10) + 1;
        txtMul2.setText(Integer.toString(random2));

        val sum = random1 * random2

        val posit = Random.nextInt(3) + 1;
        if (posit == 1) {
            btnMulti1.setText(Integer.toString(sum));
            btnMulti2.setText(Integer.toString(sum + 1));
            btnMulti3.setText(Integer.toString(sum + 2));
        } else if (posit == 2) {
            btnMulti1.setText(Integer.toString(sum - 1));
            btnMulti2.setText(Integer.toString(sum));
            btnMulti3.setText(Integer.toString(sum + 1));

        } else {
            btnMulti1.setText(Integer.toString(sum - 2));
            btnMulti2.setText(Integer.toString(sum - 1));
            btnMulti3.setText(Integer.toString(sum));
        }


        btnMulti1.setOnClickListener {
            if (btnMulti1.text.toString().toInt() == sum) {
                txtAns3.setText("ถูกต้อง")
                txtAmoutCorrect3.text =
                    (txtAmoutCorrect3.text.toString().toInt() + 1).toString()
            } else {
                txtAns3.setText("ผิด")
                txtAmoutWrong3.text = (txtAmoutWrong3.text.toString().toInt()).toString()
            }
        }
        btnMulti2.setOnClickListener {
            if (btnMulti2.text.toString().toInt() == sum) {
                txtAns3.setText("ถูกต้อง")
                txtAmoutCorrect3.text =
                    (txtAmoutCorrect3.text.toString().toInt() + 1).toString()
            } else {
                txtAns3.setText("ผิด")
                txtAmoutWrong3.text =
                    (txtAmoutWrong3.text.toString().toInt() + 1).toString()
            }
        }
        btnMulti3.setOnClickListener {
            if (btnMulti3.text.toString().toInt() == sum) {
                txtAns3.setText("ถูกต้อง")
                txtAmoutCorrect3.text =
                    (txtAmoutCorrect3.text.toString().toInt() + 1).toString()
            } else {
                txtAns3.setText("ผิด")
                txtAmoutWrong3.text =
                    (txtAmoutWrong3.text.toString().toInt() + 1).toString()
            }
        }
    }
}