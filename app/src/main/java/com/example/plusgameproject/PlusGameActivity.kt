package com.example.plusgameproject

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.TextView
import kotlinx.android.synthetic.main.activity_plus_game.*
import kotlin.random.Random

class PlusGameActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_plus_game)
//        val correct:String = intent.getStringExtra("correct")
//        val txtAmoutCorrect = findViewById<TextView>(R.id.txtAmoutCorrect)
//        txtAmoutCorrect.text = correct
        Play()
        btnNext.setOnClickListener {
            Play()
            txtAns.setText(" ")


        }

        btnHome1.setOnClickListener {



            val intent = Intent()
            intent.putExtra("correct", txtAmoutCorrect.text);
            intent.putExtra("wrong", txtAmoutWrong.text);
            setResult(RESULT_OK, intent);
            finish();




        }

    }

    private fun Play() {
        val random1 = Random.nextInt(10) + 1;
        txt1.setText(Integer.toString(random1));

        val random2 = Random.nextInt(10) + 1;
        txt2.setText(Integer.toString(random2));


        val sum = random1 + random2

        val posit = Random.nextInt(3) + 1;
        if (posit == 1) {
            btnSum1.setText(Integer.toString(sum));
            btnSum2.setText(Integer.toString(sum + 1));
            btnSum3.setText(Integer.toString(sum + 2));
        } else if (posit == 2) {
            btnSum1.setText(Integer.toString(sum - 1));
            btnSum2.setText(Integer.toString(sum));
            btnSum3.setText(Integer.toString(sum + 1));

        } else {
            btnSum1.setText(Integer.toString(sum - 2));
            btnSum2.setText(Integer.toString(sum - 1));
            btnSum3.setText(Integer.toString(sum));
        }

        btnSum1.setOnClickListener {
            if (btnSum1.text.toString().toInt() == sum) {
                txtAns.setText("ถูกต้อง")
                txtAmoutCorrect.text = (txtAmoutCorrect.text.toString().toInt() + 1).toString()
            } else {
                txtAns.setText("ผิด")
                txtAmoutWrong.text = (txtAmoutWrong.text.toString().toInt()).toString()
            }
        }
        btnSum2.setOnClickListener {
            if (btnSum2.text.toString().toInt() == sum) {
                txtAns.setText("ถูกต้อง")
                txtAmoutCorrect.text = (txtAmoutCorrect.text.toString().toInt() + 1).toString()
            } else {
                txtAns.setText("ผิด")
                txtAmoutWrong.text = (txtAmoutWrong.text.toString().toInt() + 1).toString()
            }
        }
        btnSum3.setOnClickListener {
            if (btnSum3.text.toString().toInt() == sum) {
                txtAns.setText("ถูกต้อง")
                txtAmoutCorrect.text = (txtAmoutCorrect.text.toString().toInt() + 1).toString()

            } else {
                txtAns.setText("ผิด")
                txtAmoutWrong.text = (txtAmoutWrong.text.toString().toInt() + 1).toString()
            }
        }


    }




}